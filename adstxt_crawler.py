#!/usr/bin/env python

import sys
import csv
import sqlite3
import logging
import re
import pdb
import asyncio
import async_timeout
import socket
import aiohttp
from aiohttp import ClientSession
from optparse import OptionParser
from urllib.parse import urlparse
from datetime import date
from sqlalchemy import create_engine, desc
from sqlalchemy.ext.declarative import declarative_base 
from sqlalchemy import Column, Integer, String, Date, Enum
from sqlalchemy.orm import sessionmaker
from fuzzywuzzy import process
from fuzzywuzzy import fuzz

engine = create_engine('sqlite:///adstxt.db', echo=False)
Base = declarative_base()

socket.gethostbyname('')

class Adstxt(Base):
    """Model to hold relevant information for ads.txt entries"""

    __tablename__ = 'adstxt'
    id = Column(Integer, primary_key=True)
    site_domain = Column(String(255), nullable=False)
    site_id = Column(Integer, nullable=False)
    adsystem_domain = Column(String(255), nullable=False)
    adsystem_id = Column(Integer, nullable=False)
    seller_account_id = Column(String, nullable=False)
    account_type = Column(Enum('direct', 'reseller'), nullable=False)
    tag_id = Column(String)
    comment = Column(String)
    date = Column(Date)

Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
q_type = 'adsystem_domain'
q_type_id = 'adsystem_id'

r_results = []

def fuzzy_match(domain):
    """Use fuzzy matching to reasonably determine if we already have
       seen domain
    
    Arguments:
        domain {string} -- domain to lookup
    
    Returns:
        {string} -- None or fuzzy matched domain
    """

    #As list of known domains grows, this lookup will take longer
    #Should reconsider how to implement
    all_exchanges = session.query(getattr(Adstxt, q_type), getattr(Adstxt, q_type_id)).distinct().all()
    domains = {dom[0]: dom[1] for dom in all_exchanges}
    match = process.extractOne(domain, domains.keys(), scorer=fuzz.ratio, score_cutoff=70)
    if match:
        return domains.get(match[0])
    else:
        return None

def process_row_to_db(parts, host):
    """Given each line in ads.txt, create DB entry
    
    Arguments:
        parts {list} -- List of required and optional ads.txt entry, defined by IAB
        host {string} -- domain host
    """

    adsystem_domain = parts[0].lower()
    seller_account_id = parts[1].lower()
    try:
        account_type = parts[2].lower()
    except IndexError as ie:
        return #this is required per IAB specs
    try:
        tag_id = parts[3].lower()
    except IndexError as ie:
        logging.debug('No tag_id: {}'.format(ie))
        tag_id = None
    try:
        comment = ' '.join(parts[4:])
    except IndexError as ie:
        logging.debug('No comments: {}'.format(ie))
        comment = None

    if len(adsystem_domain) > 3 and seller_account_id and account_type in ['direct', 'reseller']:
        site_id = None
        adsystem_id = None
        logging.debug( "%s | %s | %s | %s | %s | %s | %s" % (host, date.today(), adsystem_domain, seller_account_id, account_type, tag_id, comment))
        site_exists = session.query(Adstxt).filter_by(site_domain=host).first()
        if site_exists:
            site_id = site_exists.site_id
        if not site_id:
            last_added = session.query(Adstxt.site_id).order_by(
                desc(Adstxt.site_id)).first()
            site_id = last_added.site_id + 1
        if not site_id:
            site_id = 1
        adsystem_match = fuzzy_match(adsystem_domain)
        if adsystem_match:
            adsystem_id = adsystem_match
        if not adsystem_id:
            query = session.query(Adstxt.adsystem_id).order_by(desc(Adstxt.adsystem_id)).first()
            adsystem_id = query.adsystem_id + 1
        if not adsystem_id:
            adsystem_id = 1
        adstxt = Adstxt(
            site_domain=host,
            site_id=site_id,
            adsystem_domain=adsystem_domain,
            adsystem_id=adsystem_id,
            seller_account_id=seller_account_id,
            account_type=account_type,
            tag_id=tag_id,
            comment=comment,
            date=date.today()
            )
        session.add(adstxt)
        session.commit()
        sys.stdout.write("Committed {}                          \r".format(host))
        sys.stdout.flush()
    else:
        return

async def run(url, host):
    """Method to batch requests asynchronously
    
    Arguments:
        url {string} -- /ads.txt url to GET
        host {[type]} -- domain host to store
    """

    myheaders = {
        'User-Agent': 'AdsTxtCrawler/1.0; +https://github.com/InteractiveAdvertisingBureau/adstxtcrawler',
        'Accept': 'text/plain',
    }
    connector = aiohttp.TCPConnector(verify_ssl=False)
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(url, headers=myheaders) as resp:
                if resp.status == 200:
                    r_results.append({'resp': resp, 'text': await resp.read(), 'host': host})
                else:
                    logging.warning(
                        "{} returns {}".format(resp.url, resp.status_code))
        except Exception as e:
            logging.warning('Error getting {}'.format(url))

def parse_results(result):
    """Parse results of the requests and either store relevant information or
       move on.
    
    Arguments:
        result {dict} -- result of the GET with additional information
    """

    if not result.get('resp').url.human_repr().endswith('/ads.txt'):
        logging.warning("URL ({}) redirects to not /ads.txt".format(result.get('resp').url))
        return

    # HTML content, probably a 404 with the wrong return code
    if re.search(b'(<html|<head|<script)', result.get('text')):
        logging.warning("{} looks like HTML, skipping".format(result.get('resp').url))
        return

    # some line should contain schema-appropriate results
    if not re.search(b'^([^,]+,){2,3}?[^,]+$', result.get('text'), re.MULTILINE):
        logging.warning(
            "{} has nothing schema appropriate, skipping".format(
                result.get('resp').url))
        return

    logging.debug("-------------")
    logging.debug(result.get('resp').headers)
    logging.debug("-------------")
    logging.debug("%s" % result.get('text'))
    logging.debug("-------------")

    text = result.get('text')
    lines = text.splitlines()
    lines = [x.strip().decode("utf-8") for x in lines]
    lines = filter(None, lines)
    #For each line in ads.txt, process to DB
    for line in lines:
        if line.startswith('#'):
            continue
        logging.debug("DATA:  %s" % line)
        search = re.search('[, \n]', line)
        if search:
            parts = line.split(search.group())
        parts = [x.strip() for x in parts]
        if parts:
            process_row_to_db(parts, result.get('host'))

def load_url_queue(csvfilename):
    """Create a queue of validated URLs to process from the target file
    
    Arguments:
        csvfilename {string} -- List of urls to process
    
    Returns:
        dict -- 
    """

    url_queue = {}
    with open(csvfilename, 'rb') as textfile:
        urllist = textfile.read().splitlines()
        for url in urllist:
            search = re.search(b'\.\w{2,}', url)
            try:
                url = url.decode('idna')
            except UnicodeDecodeError as ude:
                logging.warning(
                    "{} can't be encoded to ascii".format(url))
                continue
            if not search:
                logging.warning('{} not a valid URL'.format(url))
                continue
            if len(url) < 1 or url.startswith( '#' ):
                continue
            if  "http:" in url or "https:" in url:
                logging.info( "URL: %s" % url)
                try:
                    parsed_uri = urlparse(url)
                    host = parsed_uri.netloc
                except UnicodeDecodeError as ude:
                    logging.warning("{} can't be encoded to ascii".format(url))
                    continue
            else:
                host = url
                logging.info( "HOST: %s" % host)
            ads_txt_url = 'http://{}/ads.txt'.format(host)
            logging.info("  pushing %s" % ads_txt_url)
            url_queue[host] = ads_txt_url
    return url_queue

def main():
    if options.target_filename:
        url_queue = load_url_queue(options.target_filename)
        loop = asyncio.get_event_loop()
        # urls = [x for x in url_queue.values()]
        loop.run_until_complete(
            asyncio.gather(*(run(url, host) for url, host in url_queue.items())
        )
        pending_tasks = asyncio.Task.all_tasks()
        loop.run_until_complete(asyncio.gather(*pending_tasks))
        for result in r_results:
            parse_results(result)
    else:
        arg_parser.print_help()
        exit(1)
    logging.warning("Finished.")

#### MAIN ####
arg_parser = OptionParser()
arg_parser.add_option(
    "-t",
    "--targets",
    dest="target_filename",
    help="list of domains to crawl ads.txt from (required)",
    metavar = "FILE"
)
arg_parser.add_option(
    "-v",
    "--verbose",
    dest="verbose",
    action='count',
    help = "Increase verbosity (specify multiple times for more) (optional)"
)

(options, args) = arg_parser.parse_args()
if len(sys.argv) == 1:
    arg_parser.print_help()
    exit(1)
log_level = logging.WARNING  # default
if options.verbose == None:
    pass
elif options.verbose == 1:
    log_level = logging.INFO
elif options.verbose >= 2:
    log_level = logging.DEBUG
logging.basicConfig(filename='adstxt_crawler.log', level=log_level,format='%(asctime)s %(filename)s:%(lineno)d:%(levelname)s  %(message)s')
if __name__ == '__main__':
    main()
